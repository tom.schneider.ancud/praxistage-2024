<%@ page import="java.util.List" %>
<%@ page import="de.ancud.praxistage.entities.MessageEntity" %>


<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="index.css" />
    <title>Our Space Travelling Company</title>
</head>
<body>
    <div id="logo">
        <a href="/">
            <img src="img/logo.png" alt="Rocket Logo" />
        </a>
    </div>


    <div id="messages">
        <h2>Nachrichten</h2>
        <%
            List<MessageEntity> messages = (List<MessageEntity>) request.getAttribute("list");

            for(MessageEntity messageObject : messages) {
                String name = messageObject.getFullName();
                String message = messageObject.getMessage();

        %>
                <div class="message">
                    <p><%= name %></p>
                    <p><%= message %></p>
                </div>
        <% } %>
    </div>
</body>
</html>
