package de.ancud.praxistage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PraxistageApplication {

	public static void main(String[] args) {
		SpringApplication.run(PraxistageApplication.class, args);
	}

}
