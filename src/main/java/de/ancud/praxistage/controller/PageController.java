package de.ancud.praxistage.controller;


import de.ancud.praxistage.entities.MessageEntity;
import de.ancud.praxistage.repositories.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;


@Controller
public class PageController {

    @Autowired
    MessageRepository messageRepository;


    @GetMapping ({"","/","/index"})
    public String index(){

        return "index.htm";
    }

    @GetMapping("/board")
    public String board(Model model){

        try {
            List<MessageEntity> listMessages = messageRepository.findAll();

            model.addAttribute("list", listMessages);
        }catch (Exception e){
            model.addAttribute("exception",e);
        }

        return "/WEB-INF/jsp/board.jsp";
    }


}
