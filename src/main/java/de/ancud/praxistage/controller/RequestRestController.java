package de.ancud.praxistage.controller;


import de.ancud.praxistage.entities.MessageEntity;
import de.ancud.praxistage.repositories.MessageRepository;
import de.ancud.praxistage.ResponeObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class RequestRestController {

    @Autowired
    MessageRepository messageRepository;

    @PostMapping("/saveMessage")
    public ResponeObject saveMessage(@RequestBody Map<String,String> requestObject){

        ResponeObject responeObject = new ResponeObject();
        MessageEntity message = new MessageEntity();

        try{

            message.setFullName(requestObject.get("fullname"));
            message.setMessage(requestObject.get("message"));

            messageRepository.save(message);

            responeObject.setSuccessfull(true);

        }
        catch (Exception e ){
            responeObject.setSuccessfull(false);
        }

        return responeObject;
    }

    @GetMapping("/getMessages")
    public List<MessageEntity> getMessages(){
        return messageRepository.findAll();
    }
}

