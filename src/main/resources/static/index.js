
async function saveMessage (){

    const getName = document.getElementById("fullname").value;
    const getMessage = document.getElementById("message").value;

    const parameters = {
        fullname:getName,
        message:getMessage
    }

    const response = await fetch("http://localhost:8080/saveMessage",{
        method:"POST",
        headers:{
            "Content-Type": "application/json"
        },
        body: JSON.stringify(parameters)
    })

    console.log(response);
}

document.getElementById("submit").addEventListener("click",function(event){
    event.preventDefault();
    saveMessage();

    // Fade out Contact Form
    document.querySelector("body").classList.add("submitted");
});
