# Praxistage @ Ancud IT

## Prerequisites

### IDE installed
Please install the IDE of your choice. Its not ready mandatory, but it is recommended to explore the code in the best possible.
If you only want to run the project, without exploring the code, you can run the maven command to accomplish this.
(See chapter maven command for running the project)

Examples of IDE's are IntelliJ Community Edition, Eclipse, Visual Studio Code
We used IntelliJ Community Edition for building the project.

### JDK 17
You need Java JDK 17 to be installed on your computer.
It is important, that it is Java Development Kit (JDK) and NOT Java Runtine Environment (JRE).
The JDK 17 can be downloaded here:
https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html

The JDK 17 has to be configured in your IDE of your choice.

You also have to set the operating system environment variable "JAVA_HOME" to your JDK installation folder.

### Mysql 8 Server
Mysql 8 has to be installed on your computer and should be running on port 3306.
As for the standard installation it should have a root user with password "123456" set.
you can change the password configuration for your needs in the application.properties file.

### Mysql Database
You have to create a schema for the praxistage project. It has to have the name "praxistage" and it should be UTF-8 encoding.

### Maven
Maven has to be installed on your system, if your IDE is not coming with maven built-in.
Usually maven is included in the IDE. First you can try without installing maven separately 
and only on failing install it and retry.

## Running the project with your IDE - Option A
In your IDE select the pom.xml file in the root directory and choose "Run Maven -> clean install"
This menu call is the example for IntelliJ.
After this step, see chapter Accessing the project in the browser.

## Run the .jar file in run_me folder
To run the showcase (this project), you can simply double click the "Praxistage-0.0.1-SNAPSHOT.jar" file or use the java command to run the .jar file
After this step, see chapter Accessing the project in the browser.

## Accessing the project in the browser
Open up a browser of your choice and visit the following url:
http://localhost:8080/

## Troubleshooting
Getting the error ...IllegalArgumentException: ... must be a regular file is a problem of the path of the project.
Try putting the project on C:\ directly or a different folder (not Desktop)
